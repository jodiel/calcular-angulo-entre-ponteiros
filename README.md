# Calcular ângulo entre ponteiros

O ângulo de 00:15h foi alterado de 45 para 90 graus para seguir uma lógica em relação ao ângulo de 180 graus. O ponteiro dos minutos se move 6 graus por minuto, essa informação se obtém ao dividir os 360 graus do relógio por 60 minutos e o mesmo ocorre com as horas ao dividir 360 por 12, resultando em 30 graus por hora.
Também foi levado em consideração que se o ponteiro das horas não estiver em um horário exato como 12:00h, ele fica um pouco mais a frente, e portanto o angulo de 00:15h por exemplo, resulta em 83 graus.

